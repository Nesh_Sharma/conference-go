import requests
from conference_go.keys import PEXEL_KEY, OPEN_WEATHER_API_KEY

def getCityPhoto(city):
     response = requests.get(
          url=f'https://api.pexels.com/v1/search?query={city}',
          headers={
                "Authorization": PEXEL_KEY
          }
     )
     content = response.json()
     try:
          return content["photos"][0]["src"]["original"]
     except (KeyError, IndexError):
          return None

def getWeatherData(city, state):
     geo_parameters = {
          "q": f"{city},{state},US",
          "limit": 1,
          "appid": OPEN_WEATHER_API_KEY,
     }
     geo_url = "http://api.openweathermap.org/geo/1.0/direct"
     response = requests.get(
          geo_url, params=geo_parameters
     )
     content = response.json()
     try:
         lat = content[0]["lat"]
         lon = content[0]["lon"]
     except (KeyError, IndexError):
          return None
     
     weather_parameters = {
          "lat": lat,
          "lon": lon,
          "appid": OPEN_WEATHER_API_KEY,
          "units": "imperial"
     }
     weather_url = "https://api.openweathermap.org/data/2.5/weather"
     response = requests.get(
          weather_url, params=weather_parameters
     )
     content = response.json()
     try:
          description = content["weather"][0]["description"]
          temperature = content["main"]["temp"]
     except(KeyError, IndexError):
          return None
     
     return {"description": description, "temperature": temperature}